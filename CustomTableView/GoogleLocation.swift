//
//  GoogleLocation.swift
//  CustomTableView
//  Created by Deepak mahadev on 17/04/18.
//  Copyright © 2018 deepak. All rights reserved.


import Foundation
import UIKit
import MapKit
import GooglePlacesSearchController
import CoreLocation



class GoogleLocation:UIViewController,UINavigationControllerDelegate,
UITextFieldDelegate,UIPickerViewDelegate, UIPickerViewDataSource{
    

   // @IBOutlet weak var fromdate: UITextField!
    var datePicker : UIDatePicker!
    
    var controller: GooglePlacesSearchController!
    
   // @IBOutlet weak var categoryUi: UITextField!
    
    var lati: Double = 0.0
    var lont: Double = 0.0
    
    @IBOutlet weak var categoryUi: UITextField!
    
    @IBOutlet weak var listofitem: UIPickerView!
    //for date
    let date = Date()
    let formatter = DateFormatter()
    
    let list = ["MarriageHall","PartyHall","MeetingHall","conferenceHall",
                "ExhibitionHall","EventHall","Beautician","Band set","caterers","Coconut Merchants",
                "Coffee Suppliers","Condiments","Dairy Product","Detective Services",
                "Entertainers & Event Oraganiser","Ethicwear","Flower Decorators","Furniture",
                "Fruit Merchants","Furniture", "GasEquipmentService",
                "GeneratorHires","GentsBeautySaloon", "GiftItems","Horseprovider",
                "Hotels","IcecreamSuppliers","JewelleryHires","Lighting","Marriage Bureaus","Matrimonial Services",
                "Mehendi","Milk Suppliers","Marriage Decorators","Music",
                "Mineral water Suppliers","Nadaswara", "Nursery Flower pots","Orchestra","Pan Suppliers"
          ,"Photographers","Plaintainleaf","plasticpaperBags", "PoojaItems","Printers",
         "Provisions","Resorts","Suiting","SilkSarees","Soft Drinks","Sweets","Shamiyana Hires","Tents Suppliers",
         "Tours & Travellers", "turbans","Videography","Vessels","Vegtable Suppliers","Water Suppliers","Wedding cards"]
    
    
    @IBAction func searchhall(_ sender: Any) {
        
        let storyborar = UIStoryboard(name: "Main", bundle: nil)
        
        let viewcont = storyborar.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        
        let lattitude: String = String(lati)
        let longitude: String = String(lont)
        
        print(lattitude)
        
        viewcont.lat = lattitude
        viewcont.lon = longitude
        viewcont.cat = categoryUi.text!
        
        self.navigationController?.pushViewController(viewcont, animated: true)
        
        
    }
    
    

    @IBOutlet weak var todate: UILabel!
   // @IBOutlet weak var fromdate: UILabel!
    
    @IBOutlet weak var fromdate1: UILabel!
    
    let  googleapinew = "AIzaSyBKi3N3MzXX871SGn7VYo1rQwireHKRVUo"
    
    lazy var placesSearchController: GooglePlacesSearchController = {
        let controller = GooglePlacesSearchController(delegate: self,
        apiKey: "AIzaSyBKi3N3MzXX871SGn7VYo1rQwireHKRVUo",placeType: .address )
         return controller
    }()


    //@IBAction func searchAddress(_ sender: Any) {
       //   present(placesSearchController, animated: true, completion: nil)
   // }
    
    
    
    
    @IBOutlet weak var searchAddress: UILabel!
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        
        present(placesSearchController, animated: true, completion: nil)
        
        print("tap working")
    }
    

    override func viewDidLoad() {
        
        //for UILabel
        let tap = UITapGestureRecognizer(target: self, action: #selector(GoogleLocation.tapFunction))
        searchAddress.isUserInteractionEnabled = true
        searchAddress.addGestureRecognizer(tap)
    
        
        //fromdate.delegate = self
        fromdate1.layer.borderWidth = 0.5
        fromdate1.layer.borderColor = UIColor.gray.cgColor
        
        todate.layer.borderWidth = 0.5
        todate.layer.borderColor = UIColor.gray.cgColor
       //for date set on uilabel
        let today = Date()
        formatter.dateFormat = "dd.MM.yyyy"
        let result = formatter.string(from: date)
        let tomorr = Calendar.current.date(byAdding: .day, value: 1, to: today)
        let tommrrow =  formatter.string(from: tomorr!)
        print(tommrrow)
        print(result)
        fromdate1.text = result
        todate.text! = tommrrow
        
        let str1 = today
        
        print("datehello" , str1)
       
    
       

    }
    

    
    override func didReceiveMemoryWarning() {
        

    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
        
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        
        return list.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        self.view.endEditing(true)
        return list[row]
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.categoryUi.text = self.list[row]
        self.listofitem.isHidden = false
        
    }
    
    
    
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//
//        //self.pickUpDate(self.fromdate)
//
//        if textField == self.categoryUi {
//            self.listofitem.isHidden = false
//            //if you dont want the users to se the keyboard type:
//
//            textField.endEditing(true)
//        }
//
//    }
    
    
    
    //for datepicker
    //MARK:- Function of datePicker
    
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        self.pickUpDate(self.textField_Date)
//    }
    
    func pickUpDate(_ textField : UITextField){

        // DatePicker
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePickerMode.date
        textField.inputView = self.datePicker

        let todaysDate = Date()

        datePicker.minimumDate = todaysDate

        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()

        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(GoogleLocation.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(GoogleLocation.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar

    }
//
//    // MARK:- Button Done and Cancel
    func doneClick() {
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .medium
        dateFormatter1.timeStyle = .none
        fromdate1.text = dateFormatter1.string(from: datePicker.date)
        fromdate1.resignFirstResponder()
    }
    func cancelClick() {
        fromdate1.resignFirstResponder()
    }

//
//    //for return key function
    func textFieldShouldReturn(_ textField : UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
    
}

extension GoogleLocation: GooglePlacesAutocompleteViewControllerDelegate {
    func viewController(didAutocompleteWith place: PlaceDetails) {
        
         lati  = (place.coordinate?.latitude)!
         lont  = (place.coordinate?.longitude)!
        
        
        print(place.formattedAddress)
        print(lont)
        
        print(lati)
        placesSearchController.isActive = false
        searchAddress.text! = place.formattedAddress
      
        
        
     
    }
}

