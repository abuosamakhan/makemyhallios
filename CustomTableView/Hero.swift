//
//  Hero.swift
//  CustomTableView
//
//  Created by deepak on 30/03/18.
//  Copyright © 2017 Belal Khan. All rights reserved.
//

class Hero {
    
    var name: String?
    var halladdress: String?
    var imageUrl: String?
    var phonenumber: String?
    var halllatitude: String?
    var halllongitude: String?
    var hallprice: String?
    
    
    init(name: String?, halladdress: String?, imageUrl: String?, phonenumber: String?,halllatitude: String?, halllongitude: String?,hallprice: String?) {
        self.name = name
        self.halladdress = halladdress
        self.imageUrl = imageUrl
        self.phonenumber = phonenumber
        self.halllatitude = halllatitude
        self.halllongitude =  halllongitude
        self.hallprice =  hallprice

    }
}
