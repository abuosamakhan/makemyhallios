//
//  BannerClass.swift
//  CustomTableView
//
//  Created by Deepak mahadev on 23/04/18.
//  Copyright © 2018 deepak. All rights reserved.
//

import Foundation

class BannerClass {
    
    
    var name: String?
    var team: String?
    var imageUrl: String?
    
    init(name: String?, imageUrl: String?) {
        self.name = name
        //self.team = team
        self.imageUrl = imageUrl
    }
    
    
    
    
}
