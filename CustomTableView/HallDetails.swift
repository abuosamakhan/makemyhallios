
//  HallDetails.swift
//  CustomTableView
//  Created by Deepak mahadev on 29/03/18.
//  Copyright © 2018 . All rights reserved.


import Foundation
import UIKit
import MapKit
import GooglePlacesSearchController
import Alamofire

class HallDetails :UIViewController{
    var getname = String()
    var getimage = String()
    var getaddress = String()
    var valueToPass: String!
    var getphonenumber = String()
    var gethalllatitude = String()
    var gethalllongitude =  String()
    var gethallprice = String()
    let discountstr: String = "0.0"
    var image = UIImage()
    
    
    @IBOutlet weak var discount: UILabel!
    
    @IBOutlet weak var totalpayable: UILabel!
    
    @IBOutlet weak var totalprice: UILabel!
    
    
    @IBAction func hallbooking(_ sender: Any) {
    
        let stroyborad = UIStoryboard(name: "Main", bundle: nil)
        let booking = stroyborad.instantiateViewController(withIdentifier: "BookingDetails") as! BookingDetails
        booking.gethallname1 = getname
        booking.gethallprice1 = gethallprice
        booking.getAdd1 = getaddress
        booking.gethallphonenumber1 = getphonenumber
        self.navigationController?.pushViewController(booking, animated: true)
        
    }
    
    
    @IBAction func callhall(_ sender: Any) {
        if let url = URL(string: "tel://\(getphonenumber)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        else {
            print("Your device doesn't support this feature.")
        }
    }
    @IBAction func mapbtn(_ sender: Any) {
        print(gethalllatitude)
        print(gethalllongitude)
        let storyborad = UIStoryboard(name: "Main", bundle: nil)
        let mapview  = storyborad.instantiateViewController(withIdentifier: "MapviewClass") as! MapviewClass
        
        mapview.halllat = gethalllatitude
        mapview.halllon = gethalllongitude
        self.navigationController?.pushViewController(mapview, animated: true)
    }
    
    @IBOutlet weak var halladdress: UILabel!
    @IBOutlet weak var hallname: UILabel!

    //@IBOutlet weak var hallnumber: UILabel!
    //@IBOutlet weak var hallname: UILabel!
    @IBOutlet weak var imageviewhall: UIImageView!
   // @IBOutlet weak var hallname1: UITextView!
    //@IBOutlet weak var imageviewhall: UIImageView!
  //  @IBOutlet weak var hallnumber: UILabel!
    //@IBOutlet weak var hallname: UILabel!
    //@IBOutlet weak var imageviewhall: UIImageView!
    
   // @IBOutlet var hallname: UIView!
    
  //  @IBOutlet weak var labelname: UILabel!
    
    
    override func viewDidLoad() {
        
        //print("row::"+""+getimage)
        discount.text! = "\u{20B9}" + discountstr
        totalprice.text! = "\u{20B9}" + gethallprice
        totalpayable.text! = "\u{20B9}" + gethallprice
        
        Alamofire.request(getimage).responseData{
            response in
            debugPrint(response)
            if let image = response.result.value {
                self.imageviewhall.image = UIImage(data: image)
            }
        }
        print(getimage)
        //imageviewhall.d
        
        //let ImageView = UIImageView()
       // let data = NSData(contentsOfURL: NSURL(string: getimage)! as URL)
       // imageviewhall.image = UIImage(data: data!)

        //imageviewhall.image = getimage
    
        hallname.text!=getname
        halladdress.text!=getaddress
        //hallname1.text!=getnumber
        
    
}
    
    override func didReceiveMemoryWarning() {
        
        
        
    }
    
    
}
