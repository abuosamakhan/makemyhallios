
//  HomeClass.swift
//  CustomTableView
//  Created by Deepak mahadev on 13/04/18.
//  Copyright © 2018 deepak. All rights reserved.


import Foundation
import UIKit
import Alamofire


class HomeClass: UIViewController,UISearchBarDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate{
    
     //let URL_GET_DATA = "https://www.makemyhall.com/m/hallcate.php"
    let URL_GET_DATA = "https://www.makemyhall.com/m/hallservices.php"
    
    var getlat = String()
    var getlon = String()
    
    var lat:String=""
    var lon:String=""
    //let cat:String="MarriageHall"
    var bannerclass = [BannerClass]()
    
    var imagearray = [UIImage(named: "delhicon.jpg"),UIImage(named: "delhicon.jpg"),UIImage(named: "delhicon.jpg")
        ,UIImage(named: "delhicon.jpg")]
    
    @IBOutlet weak var serachbar: UISearchBar!
    
    @IBOutlet weak var collectionviewBanner: UICollectionView!
    
    
    
    @IBAction func viewallservices(_ sender: Any) {
        
        
        let viewser = UIStoryboard(name: "Main", bundle: nil)
        
        let allservices = viewser.instantiateViewController(withIdentifier: "ShowServicesList") as! ShowServicesList
        
        
        allservices.getlatitude = getlat
        allservices.getlongitude = getlon
        
    
        self.navigationController?.pushViewController(allservices, animated: true)
        
        
    }
    
    
    
    
    @IBAction func finance(_ sender: Any) {
        
         self.showToast(message: "comming soon")
        

    }
    @IBAction func birthday(_ sender: Any) {
        
         self.showToast(message: "comming soon")

    }
    
    
    
    
//    @IBAction func traveller(_ sender: Any) {
//
//
//
//
//    }


    @IBAction func traveller(_ sender: Any) {
        
          self.showToast(message: "comming soon")
    }
    
    @IBAction func eventhall(_ sender: Any) {

        let storyboard = UIStoryboard(name: "Main" , bundle: nil)
        let dec = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController

        dec.lat = getlat
        dec.lon = getlon
        dec.cat = "EventHall"

        self.navigationController?.pushViewController(dec, animated: true);


    }


    @IBAction func conferencehall(_ sender: Any) {

        let storyboard = UIStoryboard(name: "Main" , bundle: nil)
        let dec = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController

        dec.lat = getlat
        dec.lon = getlon
        dec.cat = "ConferenceHall"

        self.navigationController?.pushViewController(dec, animated: true);



    }

    @IBAction func meetinghall(_ sender: Any) {

        let storyboard = UIStoryboard(name: "Main" , bundle: nil)
        let dec = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController

        dec.lat = getlat
        dec.lon = getlon
        dec.cat = "MeetingHall"

        self.navigationController?.pushViewController(dec, animated: true);


    }



    @IBAction func exhibitionhall(_ sender: Any) {

        let storyboard = UIStoryboard(name: "Main" , bundle: nil)
        let dec = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController

        dec.lat = getlat
        dec.lon = getlon
        dec.cat = "ExhibitionHall"

        self.navigationController?.pushViewController(dec, animated: true);



    }

    @IBAction func partyhall(_ sender: Any) {


        let storyboard = UIStoryboard(name: "Main" , bundle: nil)
        let dec = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController

        dec.lat = getlat
        dec.lon = getlon
        dec.cat = "partyHall"
        self.navigationController?.pushViewController(dec, animated: true);

    }
    
    
    @IBAction func marriagehall(_ sender: Any) {

        let storyboard = UIStoryboard(name: "Main" , bundle: nil)
        let dec = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        dec.lat = getlat
        dec.lon = getlon
        dec.cat = "MarriageHall"

        self.navigationController?.pushViewController(dec, animated: true);

        }
//
    
    
    
    
    
   // @IBOutlet weak var marriagehall: UIButton!

   
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return bannerclass.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        print("rowselected: \(indexPath.row)")
        
    
                let storyboard = UIStoryboard(name: "Main" , bundle: nil)
                let dec = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        
                let bannerimg1: BannerClass
                 bannerimg1 = bannerclass[indexPath.row]
        
             dec.lat = getlat
             dec.lon = getlon
             dec.cat = bannerimg1.name!
        
        
        
    
         
//                let bannerclass1: BannerClass
//                bannerclass1 = bannerclass[indexPath.row]
//
//                //dec.getimage = hero.imageUrl! as! Image
//
//                //        dec.getimage = hero.imageUr
//
//                dec.getimage = hero.imageUrl!
//                dec.getname  = hero.name!
               // dec.getnumber = bannerclass1.halladdress!
                // print(hero.imageUrl)
                self.navigationController?.pushViewController(dec, animated: true);
        
    
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageSliderCollectionViewCell",for:indexPath) as! ImageSliderCollectionViewCell
        
//        let hero: BannerClass
//        hero = bannerclass[indexPath.row]
        
        
        //
        //cell.bannerimage.image = imagearray[indexPath.row]
        
//        let storyboard = UIStoryboard(name: "Main" , bundle: nil)
//        let dec = storyboard.instantiateViewController(withIdentifier: "HallDetails") as! HallDetails
//
//        let bannerclass1: BannerClass
//        bannerclass1 = bannerclass[indexPath.row]
//
//        //dec.getimage = hero.imageUrl! as! Image
//
//        //        dec.getimage = hero.imageUr
//
//        dec.getimage = bannerclass1.imageUrl!
//
//        dec.getname  = bannerclass1.name!
//       // dec.getnumber = bannerclass1.halladdress!
//        // print(hero.imageUrl)
//        self.navigationController?.pushViewController(dec, animated: true);
        
        

        let bannerimg: BannerClass
        bannerimg = bannerclass[indexPath.row]
        
        //displaying values
        // cell.labelName.text = bannerimg.name
        cell.labelname.text = bannerimg.name
       // cell.labelTeam.text = hero.team
        
        
        
        //displaying image
        Alamofire.request(bannerimg.imageUrl!).responseData { response in
            debugPrint(response)
            
            if let image = response.result.value {
                cell.bannerimage.image = UIImage(data: image)
            }
        }
        return cell
        
    }
    

  

  //  @IBOutlet weak var CollectionViewList: UICollectionViewCell!
    

//    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//        if let text = searchBar.text {
//            // here is text from the search bar
//            print(text)
//
//            //userInput = text
//
//            // now you can call 'performSegue'
//            //performSegue(withIdentifier: "searchView", sender: self)
//        }
//    }
//
    
    
    private func setUpNavigationBarItem(){
        
        let titleView = UIImageView(image: #imageLiteral(resourceName: "logowhite"));
        titleView.frame =  CGRect(x: 0, y: 0, width: 50, height: 70)
        titleView.contentMode = .scaleAspectFill
        
        let searcbutton = UIButton(type: .system)
        searcbutton.setImage(#imageLiteral(resourceName: "icons").withRenderingMode(.alwaysOriginal), for: .normal)
        
        searcbutton.frame = CGRect(x: 10, y: 0, width: -20, height: 10)
        
        let searcbutton1 = UIButton(type: .system)
        searcbutton1.setImage(#imageLiteral(resourceName: "icons").withRenderingMode(.alwaysOriginal), for: .normal)
        searcbutton1.frame = CGRect(x: 10, y: 0, width: -20, height: 10)
       // searcbutton1.contentMode = .scaleAspectFill
       // navigationItem.rightBarButtonItem = UIBarButtonItem(customView: searcbutton)
        navigationItem.titleView = titleView
        
        navigationItem.rightBarButtonItems = [ UIBarButtonItem(customView: searcbutton)]
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(HomeClass.tappedMe))
        searcbutton.addGestureRecognizer(tap)
        searcbutton.isUserInteractionEnabled = true

        print(123)
    }
    
    
    
    
    override func viewDidLoad() {
        
        self.collectionviewBanner.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0)
        
        self.collectionviewBanner.backgroundColor = UIColor.clear
        
        
        //self.automaticallyAdjustsScrollViewInsets = false
       // self.contentInsetAdjustmentBehavior =  false
        
          setUpNavigationBarItem()
        
    
          //for seach icon
//        let img = UIImage(named: "search.png")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
//        let leftBarButtonItem = UIBarButtonItem(image: img, style: UIBarButtonItemStyle.plain, target: self, action: nil)
//        self.navigationItem.rightBarButtonItem = leftBarButtonItem
        
        
    
        
        //let item1 = UIBarButtonItem(title: "Continue", style: .plain, target: self, action: "icon.png")
        // toolBar.setItems([item1], animated: true)
        
//        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
//        imageView.frame = CGRect(x: 200.0, y: 13.0, width: 24.0, height: 24.0)
//        imageView.contentMode = .scaleAspectFit
//        let image = UIImage(named: "icon.png")
//        //imageView.frame = CGRect(x: 100.0, y: 13.0, width: 24.0, height: 24.0)
//        imageView.image = image
//        navigationItem.titleView = imageView
        
//        let tap = UITapGestureRecognizer(target: self, action: #selector(HomeClass.tappedMe))
//        .addGestureRecognizer(tap)
//        imageView.isUserInteractionEnabled = true
//
        
        //load image from server
        
        //lat =  getlat
       // lon =  getlon
        
       // print(lat)
        
        let parameters: Parameters=[
            "latitude":lat,
            "longitute":lon
        ]
        
        //fetching data from web api
        Alamofire.request(URL_GET_DATA ,method: .post, parameters: parameters).responseJSON { response in
            
            //getting json
            if let json = response.result.value {
                
                //converting json to NSArray
                //let heroesArray : NSArray  = json as! NSArray
                
                let jsonData = json as! NSDictionary
                
                //converting json to NSArray
                //  let heroesArray : NSArray  = json ["ok"] as! NSArray
                let heroesArray : NSArray  = jsonData ["ok"] as! NSArray
                
                
                //traversing through all elements of the array
                for i in 0..<heroesArray.count{

                    //adding hero values to the hero list
                      self.bannerclass.append(BannerClass(
                        name: (heroesArray[i] as AnyObject).value(forKey: "service_name") as? String,
                        //team: (heroesArray[i] as AnyObject).value(forKey: "hall_address") as? String,
                        imageUrl: (heroesArray[i] as AnyObject).value(forKey: "service_banner") as? String
                    ))
                
                    //let hero = heroesArray[0] as? [String : AnyObject]
                    //let urlstr = hero!["service_banner"] as? String
                    //let url = URL(string: urlstr!)
                
//                func beginGetImageRequest() {
//                    if let imagePath = thumbPath {
//                        request = Alamofire.request(imagePath, method: .get, parameters: [:], encoding: JSONEncoding.default)
//                            .validate { request, response, imageData in
//                                if let downloadedImage = UIImage(data: imageData!) {
//                                    self.imageView.image = downloadedImage
//                                } else {
//                                    print(response)
//                                    print(imageData)
//                                }
//                                return .success
//                        }
//                    }
//                }

               
//
//
               }
                //displaying data in tableview
                self.collectionviewBanner.reloadData()
            }
            
        }
        self.collectionviewBanner.reloadData()
        
    }
        
        
        
    
    func tappedMe()
    {

        let storyboard = UIStoryboard(name: "Main" , bundle: nil)
        let dec = storyboard.instantiateViewController(withIdentifier: "GoogleLocation") as! GoogleLocation
        self.navigationController?.pushViewController(dec, animated: true);
        print("Tapped on Image")
    }
    
    
    override func didReceiveMemoryWarning() {
        
    
    }
    
    override func viewDidLayoutSubviews() {
        //collectionviewBanner.frame = CGRect(x: 100, y: 50, width: view.frame.width, height: view.frame.height)
    }
    
    
    //function for toast
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-550, width: 150, height: 40))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }

//    override func viewDidAppear(_ animated: Bool) {
//
//        // 1
//        let nav = self.navigationController?.navigationBar
//        // 2
//        nav?.barStyle = UIBarStyle.black
//        //nav?.tintColor = UIColor.blue
//        // 3
//        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
//        imageView.contentMode = .scaleAspectFit
//        // 4
//        let image = UIImage(named: "logowhite.png")
//        imageView.image = image
//        // 5
//        navigationItem.titleView = imageView
//    }
    
    
}
    

