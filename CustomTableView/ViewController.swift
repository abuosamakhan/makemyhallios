
//  ViewController.swift
//  CustomTableView
//  Created by deepak on 30/03/18
//  Copyright © 2017 deepak. All rights reserved.



import UIKit
import Alamofire
//adding class DataSource and Delegate for our TableView
class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate{
    
   // @IBOutlet weak var currentdate: UILabel!
    //@IBOutlet weak var currentdate: UILabel!
    //@IBOutlet weak var tomorrow: UILabel!
    @IBOutlet weak var categoryitem: UILabel!
    @IBOutlet weak var tomorrow: UIButton!
    @IBOutlet weak var currentdate: UIButton!
    let helper = Helper()
    
   // @IBOutlet weak var viewindicaticator: UIActivityIndicatorView!
    
    
    @IBOutlet weak var viewindicaticator: UIActivityIndicatorView!
    
    //@IBOutlet weak var viewindicaticator: UIActivityIndicatorView!
   // @IBOutlet weak var viewindicaticator: UIActivityIndicatorView!
    
    // @IBOutlet weak var currentdate: UIButton!
    
    //the Web API URL
    let URL_GET_DATA = "https://www.makemyhall.com/m/hallcate.php"
    var selectedImage:String?
    var selectedLabel:String?
    var  valueToPass:String=""
    
   // var selectedImage:String?
    //var selectedLabel:String?
    
    
    @IBOutlet weak var categoryforhall: UIButton!
    
    //for date
    let date = Date()
    let formatter = DateFormatter()
    
    var lat:String=""
    var lon:String=""
    var cat:String=""
    //our table view
    @IBOutlet weak var tableViewHeroes: UITableView!
    
    //a list to store heroes
    var heroes = [Hero]()
    
    //the method returning size of the list
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return heroes.count
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("rowselected: \(indexPath.row)")

        let storyboard = UIStoryboard(name: "Main" , bundle: nil)
        let dec = storyboard.instantiateViewController(withIdentifier: "HallDetails") as! HallDetails

        let hero: Hero
        hero = heroes[indexPath.row]
        
          //dec.getimage = hero.imageUrl! as! Image
          //dec.getimage = hero.imageUr
          //dec.getimage = hero.imageUrl!
        
         dec.getname  = hero.name!
         dec.getaddress = hero.halladdress!
         dec.getphonenumber = hero.phonenumber!
         dec.gethalllatitude =  hero.halllatitude!
         dec.gethalllongitude = hero.halllongitude!
         dec.gethallprice = hero.hallprice!
        
         dec.getimage = hero.imageUrl!
        
        

        
        
         //let URLStr = self.url[indexPath.row] as! String
    
       // dec.getimage = self.url[indexPath.row] as! String

        // dec.getimage = hero.imageUrl
        
        // print(hero.imageUrl)
        
        self.navigationController?.pushViewController(dec, animated: true);
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath:
        IndexPath) {
        
        if indexPath.row  == heroes.count - 1{
            
            
           // moreData()
        }
        
    
    }
    
//    func moreData(){
//
//        for _ in 0...10{
//        heroes.append(heroes.last! + 1)
//
//        }
//
//    }

//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        //code to execute on click
//
//    }
    
    //the method returning each cell of the list

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ViewControllerTableViewCell
    
        
        cell.frame = UIEdgeInsetsInsetRect(cell.frame, UIEdgeInsetsMake(10, 10, 10, 10))
        
        
        //getting the hero for the specified position
        let hero: Hero
        hero = heroes[indexPath.row]
        
        
        //displaying values
        cell.labelName.text = hero.name
        cell.labelTeam.text = hero.halladdress
        cell.hallprice1.text = "\u{20B9}" + hero.hallprice!
        

        
        //displaying image
        Alamofire.request(hero.imageUrl!).responseData{
            response in
            debugPrint(response)
            if let image = response.result.value {
                cell.heroImage.image = UIImage(data: image)
            }
        }
        
        return cell
    }
    
    
  

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        //print(cat)
        
    
       // categoryforhall = cat.text!
        
        categoryforhall.setTitle(cat,for: .normal)
        
        //for todaydate
        formatter.dateFormat = "dd.MM.yyyy"
        let result = formatter.string(from: date)
        print(result)
      // currentdate.text = result
        //currentdate.setTitle("", for: <#T##UIControlState#>)
        currentdate.setTitle(result,for: .normal)
        //for border of ui
//        currentdate.layer.borderWidth = 0.5
//        currentdate.layer.borderColor = UIColor.green.cgColor
        
        //tommrow date
        let todaydate = Date()
        formatter.dateFormat = "dd.MM.yyyy"
       // let resultdate = formatter.string(from: date)
        let tomorr = Calendar.current.date(byAdding: .day, value: 1, to: todaydate)
        let tommrrowdate =  formatter.string(from: tomorr!)
         tomorrow.setTitle(tommrrowdate, for: .normal)
        // tomorrow.text! = tommrrow
//        tomorrow.layer.borderWidth = 0.5
//        tomorrow.layer.borderColor = UIColor.green.cgColor
        
        
//        let calendar = Calendar.current
//        let date1 = Date()
//
//        //let today1 = calendar.isDateInYesterday(date1)
//        //calendar.isDateInToday(date1)
//        let today1=calendar.isDateInTomorrow(date1)
//
//       // Calendar.current.isDateInWeekend(date1)
//
//        //print(calendar)
        
       // print(today1)
        
        let parameters: Parameters=[
            "latitude":lat,
            "longitute":lon,
            "category":cat
        ]
        
         viewindicaticator.startAnimating()
        //fetching data from web api
        Alamofire.request(URL_GET_DATA ,method: .post, parameters: parameters).responseJSON { response in

            //getting json
            if let json = response.result.value {

                //converting json to NSArray

                let jsonData = json as! NSDictionary

                //converting json to NSArray
                let hallArray : NSArray  = jsonData ["ok"] as! NSArray

            
                //traversing through all elements of the array
                for i in 0..<hallArray.count{

                    //adding hero values to the hero list
                    self.heroes.append(Hero(
                        name: (hallArray[i] as AnyObject).value(forKey: "Hall_name") as? String,
                        halladdress: (hallArray[i] as AnyObject).value(forKey: "hall_address") as? String,
                        imageUrl: (hallArray[i] as AnyObject).value(forKey: "hall_banner") as? String,
                        phonenumber: (hallArray[i] as AnyObject).value(forKey: "hall_contect_no") as? String,
                        halllatitude: (hallArray[i] as AnyObject).value(forKey: "hall_latitude") as? String,
                        halllongitude: (hallArray[i] as AnyObject).value(forKey: "hall_longitute") as? String,
                        hallprice: (hallArray[i] as AnyObject).value(forKey: "hall_price") as? String
                        
                    ))
                    

                }
                self.viewindicaticator.stopAnimating()
                //displaying data in tableview
                self.tableViewHeroes.reloadData()
            }

        }
        self.tableViewHeroes.reloadData()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

}

