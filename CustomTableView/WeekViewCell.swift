//
//  WeekViewCell.swift
//  CareUI
//
//  Created by Pavel Zhuravlev on 1/23/17.
//  Copyright © 2017 Care.com. All rights reserved.
//

import UIKit

protocol WeekViewCellProtocol: class {
    func fontAndColorForDate(date: Date) -> (font: UIFont, color: UIColor)
    func didSelectDate(date: Date, cell: WeekViewCell, pos: CGPoint)
    func selectedStartDate() -> Date?
    func selectedEndDate() -> Date?
}

class WeekViewCell: UITableViewCell {
    
    private let circleDiameter = CGFloat(26)

    @IBOutlet var dateLabels: [UILabel]! //[100,101,102,103,104,105,106]
    @IBOutlet var monthLabels: [UILabel]!//[200,201,202,203,204,205,206]
    @IBOutlet weak var gradientView: UIView! // for drawing selection and gradient
    
    weak var delegate: WeekViewCellProtocol!
    private var _date: Date!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        dateLabels.sort(by: { $0.tag < $1.tag })
        for dateLabel in dateLabels {
            let tapGestRecognizer = UITapGestureRecognizer(target: self, action: #selector(WeekViewCell.selectDayTapGestureRecognizer(recognizer:)))
            tapGestRecognizer.numberOfTapsRequired = 1
            dateLabel.addGestureRecognizer(tapGestRecognizer)
        }
    }
    
    func firstDayOfWeek() -> Date {
        return _date
    }
    
    func setWeekStartDate(date: Date) {
        _date = date
        updateDateLabels()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        for label in monthLabels {
            label.isHidden = true
        }
        for view in gradientView.subviews {
            view.removeFromSuperview()
        }
    }
    
    func updateDateLabels() {
        
        var startDateLabel: UILabel!
        // show numbers
        for index in 0 ..< 7  {
            let dateLabel = dateLabels[index]
            let activeDate = _date.cr_dateByAddingDays(dateLabel.tag - 100)
            let dateComponenets = activeDate.cr_daysInMonthAsTuple()
            if let delegate = delegate {
                let style = delegate.fontAndColorForDate(date: activeDate)
                dateLabel.font = style.font
                dateLabel.textColor = style.color
            }
            dateLabel.text = "\(dateComponenets.day)"
            // show month label
            if dateComponenets.day == 1 {
                for monthLabel in monthLabels {
                    if monthLabel.tag == (dateLabel.tag + 100) {
                        monthLabel.isHidden = false
                        monthLabel.text = Date.monthFor(dateComponenets.month)
                    }
                }
            }
            // show circle
            let startDate = delegate?.selectedStartDate()
            let endDate = delegate?.selectedEndDate()
            if startDate != nil && startDate == activeDate || endDate != nil && endDate == activeDate {
                let backgroundCircleView = UIView(frame: CGRect(x: 0, y: 0, width: circleDiameter, height: circleDiameter))
                backgroundCircleView.clipsToBounds = true
                backgroundCircleView.center = CGPoint(x: floor(dateLabel.center.x), y: floor(dateLabel.center.y))
                backgroundCircleView.layer.cornerRadius = circleDiameter/2
                gradientView.addSubview(backgroundCircleView)
                backgroundCircleView.backgroundColor = UIColor.blue.cgColor
                backgroundCircleView.backgroundColor = UIColor.blue.cgColor
            }
            // show gradient start/end row
            if delegate != nil && delegate!.selectedStartDate() != delegate!.selectedEndDate() {
                if let startDate = delegate?.selectedStartDate(), startDate == activeDate {
                    var frameWidth = (gradientView.frame.size.width - dateLabel.center.x)
                    if let endDate = delegate?.selectedEndDate(), endDate.timeIntervalSince(_date.cr_dateByAddingDays(6)) <= 0 {
                        startDateLabel = dateLabel
                        frameWidth = 0
                    }
                    let backgroundGradientView = UIView(frame: CGRect(x: dateLabel.center.x, y: 0, width: frameWidth, height: circleDiameter))
                    backgroundGradientView.center.y = dateLabel.center.y
                    backgroundGradientView.clipsToBounds = true
                    gradientView.insertSubview(backgroundGradientView, at: 0)
                    backgroundGradientView.backgroundColor = UIColor.colorWithHex(0xADEFD5)
                }
                if let endDate = delegate?.selectedEndDate(), endDate as Date == activeDate {
                    var frameWidth = dateLabel.center.x
                    var frameX = CGFloat(0)
                    if startDateLabel != nil {
                        frameX = startDateLabel.center.x
                        frameWidth = dateLabel.center.x - startDateLabel.center.x
                    }
                    let backgroundGradientView = UIView(frame: CGRect(x: frameX, y: 0, width: frameWidth, height: circleDiameter))
                    backgroundGradientView.center.y = dateLabel.center.y
                    backgroundGradientView.clipsToBounds = true
                    gradientView.insertSubview(backgroundGradientView, at: 0)
                    backgroundGradientView.backgroundColor = UIColor.colorWithHex(0xADEFD5)
                }
            }
        }
        // show gradient middle row
        // if week's first day is greater that selected start date
        if let startDate = delegate?.selectedStartDate(), _date.timeIntervalSince(startDate as Date) > 0 {
            if let endDate = delegate?.selectedEndDate(), _date.cr_dateByAddingDays(6).timeIntervalSince(endDate) < 0 {
                let backgroundGradientView = UIView(frame: CGRect(x: 0, y: 0, width: gradientView.frame.size.width, height: circleDiameter))
                backgroundGradientView.clipsToBounds = true
                backgroundGradientView.center.y = dateLabels[0].center.y
                gradientView.insertSubview(backgroundGradientView, at: 0)
                backgroundGradientView.backgroundColor = UIColor.colorWithHex(0xADEFD5)
            }
        }
    }
    
    @objc func selectDayTapGestureRecognizer(recognizer: UITapGestureRecognizer) {
        let label = recognizer.view!
        // inform delegate of selected date
        var pos = label.center
        if label.tag == 106 {
            pos = label.frame.origin
        }
        else if label.tag == 100 {
            pos = CGPoint(x: label.frame.origin.x + label.frame.size.width, y: 0)
        }
        delegate?.didSelectDate(date: _date.cr_dateByAddingDays(label.tag - 100), cell: self, pos: pos)
    }
    
}
