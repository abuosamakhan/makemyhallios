//
//  Payumoney.swift
//  CustomTableView
//  Created by Deepak mahadev on 14/05/18.
//  Copyright © 2018 deepak. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class Payumoney: UIViewController, WKUIDelegate{
    
    @IBOutlet weak var webviewpayumoney: WKWebView!
    
    @IBOutlet var contentView: UIView!
    
    var getpricehall = String()
    var email: String = "abu.jamia7@gmail.com"
    var amut : String = "1"
    var ph: String = "9509556193"
    var pro: String = "makemyhall"
    var name: String = "abc"
    
    override func viewDidLoad() {
        
//        let url = NSURL (string: "http://www.aatiri.com/paytm/pgRedirect.php?CUST_ID=" + email + "&TXN_AMOUNT=" + getpricehall)
        
        self.webviewpayumoney.frame = self.view.frame
        webviewpayumoney.contentMode = .scaleAspectFit
        
        //self.webviewpayumoney.delegate = self
        //deletelf.webviewpayumoney.scalesPageToFit = true
//        webviewpayumoney = WKWebView(frame:contentView.frame)
//        contentView.addSubview(webviewpayumoney)
//        constrainView(view: webviewpayumoney, toView: contentView)

        
         //let url = NSURL (string: "https://www.makemyhall.com/m/ios-payumoney/index.php")
        
       //let url = NSURL (string: "https://www.makemyhall.com/m/ios-payumoney/index.php?amount=" + amut + "&firstname=" + name + "&email=" + email + "&phone=" + ph + "&productinfo= " + pro)

          let url = NSURL (string: "http://makemyhall.com/m/ios-payumoney/index.php?amount=" + amut + "&firstname=" + name + "&email=" + email + "&phone=" + ph + "&productinfo=makemyhall")
        
         let requestObj = URLRequest(url: url! as URL)
         webviewpayumoney.load(requestObj)
    
    }
    
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webviewpayumoney = WKWebView(frame: .zero, configuration: webConfiguration)
        webviewpayumoney.uiDelegate = self
        view = webviewpayumoney
    }
    
    
    override func didReceiveMemoryWarning() {
        
    }
    
    func constrainView(view:UIView, toView contentView:UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
    }

    
}
