//
//  BookingDetails.swift
//  CustomTableView
//  Created by Deepak mahadev on 09/04/18.
//  Copyright © 2018 deepak. All rights reserved.

import Foundation
import UIKit
import Alamofire


class BookingDetails :UIViewController, UITextFieldDelegate{
    
    let URL_hall_details="https://www.makemyhall.com/m/makemyhallbooking.php"
    
    var gethallname1   = String()
    var gethallmobile1 = String()
    var gethallprice1  = String()
    var getAdd1        = String()
    var gethallphonenumber1 = String()
    let status : String = "online"
    
    @IBOutlet weak var hallnameb: UILabel!
    @IBOutlet weak var halladdressb: UILabel!
    @IBOutlet weak var hallpriceb: UILabel!
    

    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var useremailid: UITextField!
    @IBOutlet weak var usermobile: UITextField!
    
    
    
    
    
    @IBAction func sendData(_ sender: Any) {
        
        let providedEmailAddress = useremailid.text!
        
        let isEmailAddressValid = isValidEmailAddress(emailAddressString:  providedEmailAddress)
        
        
        if((username.text?.isEmpty)! || (useremailid.text?.isEmpty)! || (usermobile.text?.isEmpty)!){
            
             self.showToast(message: "Please Fill data")
            
        }
        else{
            
            if(!isEmailAddressValid){
                
               
                
                print("Email address is not valid")
                displayAlertMessage(messageToDisplay: "Email address is not valid")
                
            }
            
            else{
                
                 print("email is correct")
              
            
            let parameters: Parameters=[
                      "name":username.text!,
                      "email":useremailid.text!,
                      "phone":usermobile.text!,
                      "hallname": gethallname1,
                      "hallmobile":gethallphonenumber1,
                       "hallprice":gethallprice1,
                       "status":status]
        

            Alamofire.request(URL_hall_details, method: .post, parameters: parameters).responseJSON
                {
                    response in
                    //printing response
                    print(response)

                    //getting the json value from the server
                    if let result = response.result.value {

                        //converting it as NSDictionary
                        let jsonData = result as! NSDictionary
                        //print("JSON: \(jsonData)")
                        print(jsonData.value(forKey: "messqge") as Any)
                        self.showToast(message: "successful booking done")
                        
                        
                        let profileViewController = self.storyboard?.instantiateViewController(withIdentifier: "PaymentClass") as! PaymentClass
                        
                        
                        profileViewController.gethallname = self.gethallname1
                        profileViewController.gethalladd1 = self.getAdd1
                        profileViewController.gethallprice =  self.gethallprice1
                        
                        
                        
                        self.navigationController?.pushViewController(profileViewController, animated: true)
                        
                        self.dismiss(animated: false, completion: nil)
        
                    }
            }
            }
            
            
        }
        
    }
    
    
    override func viewDidLoad() {
        
        hallnameb.text! = gethallname1
        halladdressb.text! = getAdd1
        hallpriceb.text! =  "\u{20B9}" + gethallprice1
        
         username.delegate = self
         useremailid.delegate = self
         usermobile.delegate = self
        
        
    
    }
    
    override func didReceiveMemoryWarning() {
    
    }
    
    //for return keyboard key
    func textFieldShouldReturn(_ textField : UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
    
    
    
    //for email validation
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    func displayAlertMessage(messageToDisplay: String)
    {
        let alertController = UIAlertController(title: "Alert", message: messageToDisplay, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            
            // Code in this block will trigger when OK button tapped.
            print("Ok button tapped");
            
        }
        
        alertController.addAction(OKAction)
        
        self.present(alertController, animated: true, completion:nil)
    }
    
    
    
    
    
    
    //function for toast
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-550, width: 150, height: 40))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
}
