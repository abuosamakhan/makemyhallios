//
//  PCDateSelectionViewController.swift
//  PetCare
//
//  Created by Amresh Kumar on 20/11/15.
//  Copyright © 2015 Care.com. All rights reserved.
//

import UIKit

@objc public protocol DateSelectionViewControllerDelegate: class {
    /// For single selection - StartDate & endDate will be same
    func didSelectStartDate(date: Date, endDate: Date)
    @objc optional func didSelectStartTime(time: Date)
    @objc optional func didCancelDateSelection()
    @objc optional func didSelectDays(startDay: String, endDay: String)
}

typealias DateRange = (start: Date, end: Date)

public class DateSelectionViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, WeekViewCellProtocol {

    @IBOutlet weak var tableView: UITableView!

    // by default we select date interval with start/end dates
    public var needTimeSelectionAfterDateSelection = false
    public var showSingleTimePicker = false
    public var singleDateSelection = true
    
    public weak var delegate: AnyObject?
    var comingFromHireFlow = false
    private var today = Date().cr_truncateTime()
    private var startDate = Date().cr_truncateTime()
    private var endDate: Date!

    // TODO: these should be styled
    private var normalOrPastDateFont  = UIFont.init(name: "Avenir-Roman", size: 13)!
    private var todayFont             = UIFont.init(name: "Avenir-Heavy", size: 13)!
    private var normalDateColor       = UIColor.blue.cgColor
    private var pastDateColor         = UIColor.blue.cgColor
    private var todayColor            = UIColor.blue.cgColor

    @IBOutlet weak var pickupTimeButton: UIButton!
    @IBOutlet weak var headerLabel: UILabel!

    @IBOutlet weak var monthNavigateUpButton: UIButton!
    @IBOutlet weak var monthNavigateDownButton: UIButton!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint! // 256

    @IBOutlet weak var selectedDateViewWidthConstraint: NSLayoutConstraint! // 256 default, 132 condensed
    @IBOutlet weak var startDateCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var endDatePlaceholder: UILabel!
    @IBOutlet weak var startDatePlaceholder: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var startEndDateLabelContainer: UIView!
    @IBOutlet weak var monthSliderContainer: UIView!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!

    // TODO: these should become continuos gradient
    @IBOutlet var headerBackroundViews: [UIView]?
    @IBOutlet weak var headerGradientView: UIView!
    
    @IBOutlet weak var headerSeparatorView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    
    private var pickStartDatePillShown = false
    private var pickEndDatePillShown = false
    private var selectedDateRange: DateRange!
    private let dateFormatter = DateFormatter()
    
    public static func viewController(singleDateSelection: Bool? = nil) -> DateSelectionViewController {
        let storyboard: UIStoryboard = UIStoryboard(name: "DateSelection", bundle: Bundle(identifier: "com.care.CareUI"))
        let dateSelectionVC  = storyboard.instantiateViewController(withIdentifier: "dateSelectionVC") as! DateSelectionViewController
        if singleDateSelection != nil {
        dateSelectionVC.singleDateSelection = singleDateSelection!
        }
        return dateSelectionVC
    }

    @IBAction func monthNavigationButtonPressed(sender: UIButton) {
        if let visibleCellIndxedPaths = tableView.indexPathsForVisibleRows {
            if sender == monthNavigateUpButton {
                var firstVisibleRow: Int?
                for indexPath in visibleCellIndxedPaths {
                    firstVisibleRow = (firstVisibleRow == nil) ? indexPath.row : min(firstVisibleRow!, indexPath.row)
                }
                tableView.scrollToRow(at: IndexPath(row: firstVisibleRow!, section: 0), at: .bottom, animated: true)
            }
            else if sender == monthNavigateDownButton {
                var lastVisibleRow: Int?
                for indexPath in visibleCellIndxedPaths {
                    lastVisibleRow = (lastVisibleRow == nil) ? indexPath.row : max(lastVisibleRow!, indexPath.row)
                }
                tableView.scrollToRow(at: IndexPath(row: lastVisibleRow!, section: 0), at: .top, animated: true)
            }
        }
    }

    @IBAction func cancelButtonPressed(sender: UIButton) {
        if navigationController != nil {
            navigationController?.popViewController(animated: true)
        }
        else {
            dismiss(animated: true, completion: nil)
        }
        delegate?.didCancelDateSelection?()
    }

    @IBAction func pickupTimeButtonPressed(sender: UIButton) {
        if singleDateSelection {
            delegate?.didSelectStartDate(date: selectedDateRange.start, endDate: selectedDateRange.end)
           
            if navigationController != nil {
                _ = self.navigationController?.popViewController(animated: true)
            }
            else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        else {
//            let storyBoard = UIStoryboard(name: "SearchParamsPicker", bundle: nil)
//            let timePickerVC = storyBoard.instantiateViewControllerWithIdentifier("PCTimePickerVC") as! PCSearchTimePickerViewController
//            timePickerVC.delegate = delegate
//            timePickerVC.dismissDelegate = self
//            timePickerVC.selectedDate = selectedDateRange.start
//            timePickerVC.pickerMode = pickerMode
//            timePickerVC.comingFromHireFlow = comingFromHireFlow
//            timePickerVC.provider = provider
//            self.presentViewController(timePickerVC, animated: true, completion: nil)
        }
    }
    
    func getDayNameBy(stringDate: String) -> String{
        let df  = DateFormatter()
        df.dateFormat = "YYYY-MM-dd"
        let date = df.date(from: stringDate)!
        df.dateFormat = "EEEE"
        return df.string(from: date)
    }
    
    func searchButtonPresssed() {
        
        let strongSelf = self
        if navigationController != nil {
            navigationController?.popViewController(animated: true)
        }
        else {
            strongSelf.dismiss(animated: true, completion: nil)
        }

    }
    
    @IBAction func searchButtonPressed(sender: UIButton?) {
        delegate?.didSelectStartDate(date: selectedDateRange.start, endDate: selectedDateRange.end)
        yearLabel.textColor = UIColor.blueColor.CGColor
        if needTimeSelectionAfterDateSelection {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                _ = self.navigationController?.popViewController(animated: false)
                let timeSelectionVC = TimeSelectionViewController.viewController
                timeSelectionVC.delegate = self.delegate as! TimeSelectionViewControllerDelegate?
                timeSelectionVC.shouldShowStartTimeOnly = self.showSingleTimePicker
                self.present(timeSelectionVC, animated: true, completion: nil)
            }
        }
        else {
            if navigationController != nil {
                _ = navigationController?.popViewController(animated: false)
            }
            else {
                self.dismiss(animated: false, completion: nil)
            }
        }
    }

    override dynamic public func viewDidLoad() {
        super.viewDidLoad()
        if singleDateSelection {
        self.title = "Select Date"
        }
        else {
            self.title = "Select Dates"
        }
        headerBackroundViews?.forEach { $0.backgroundColor = UIColor.clear }
        if singleDateSelection {
            headerLabel.text = "Select starting Date"
            pickupTimeButton.setTitle("OK", for: .normal)
        }
        
        pickupTimeButton.isHidden = true

        startEndDateLabelContainer.isHidden = true
        monthSliderContainer.isHidden = false
        // end date will be 365days from today
        endDate = Date().cr_dateByAddingDays(365)
        monthNavigateUpButton.isEnabled = false
        monthNavigateDownButton.isEnabled = true

        if UIScreen.main.bounds.height < 568 { // 480
            tableViewHeightConstraint.constant = 204
        }
        updateMonthLabelWithDate(date: today)
//        style()
    }

    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? WeekViewCell, !pickStartDatePillShown {
            showPickStartDatePill(cell: cell)
            pickStartDatePillShown = true
        }
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "weekViewCellId", for: indexPath) as! WeekViewCell
        cell.delegate = self
        cell.setWeekStartDate(date: startDate.cr_dateByAddingDays(7 * indexPath.row).cr_firstDateOfWeek())
        return cell
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 + (endDate as Date).cr_dayOffsetFromDate(startDate)/7
    }

    // PCWeekViewCellProtocol
    func fontAndColorForDate(date: Date) -> (font: UIFont, color: UIColor) {
        if selectedDateRange != nil
            && (date.timeIntervalSince(selectedDateRange.start as Date) == 0 || date.timeIntervalSince(selectedDateRange.end as Date) == 0) {
            return (font: todayFont, color: UIColor.white)
        }
        if date.cr_isSameDayWithOtherDate(today)
        {
            return (font: todayFont, color: todayColor)
        }
        else if date.timeIntervalSince(today) < 0
        {
            return (font: normalOrPastDateFont, color: pastDateColor)
        }
        return (font: normalOrPastDateFont, color: normalDateColor)
    }

    func didSelectDate(date: Date, cell: WeekViewCell, pos: CGPoint) {
        if date.timeIntervalSince(today) < 0 {
            return
        }

        if selectedDateRange == nil && !singleDateSelection, let row = tableView.indexPath(for: cell)?.row, row > 1 {
            tableView.scrollToRow(at: IndexPath(row: row-1, section: 0), at: .top, animated: true)
        }

        if selectedDateRange == nil {
            if selectedDateRange != nil && date.timeIntervalSince(selectedDateRange.start) == 0 && date.timeIntervalSince(selectedDateRange.end) == 0 {
                selectedDateRange = nil
            }
            else {
                selectedDateRange =  (start: date, end: date)
            }
            // Shown only once
            if !pickEndDatePillShown && !singleDateSelection /* && (pickerMode == .Boarding || pickerMode == .Sitting) */ {
                if let row = tableView.indexPath(for: cell)?.row {
                    let rowForShowingPill = row > 0 ? row-1 : row+1
                    if let cell = tableView.cellForRow(at: IndexPath(row: rowForShowingPill, section: 0)) as? WeekViewCell {
                        showPickEndDatePill(pos: pos, cell: cell)
                        pickEndDatePillShown = true
                    }
                }
            }
        }
        else {
            if date.timeIntervalSince(selectedDateRange.start) > 0 && date.timeIntervalSince(selectedDateRange.end) < 0 {
                // selected date is in between existing date range
                // assume seeker wants to do fresh selection
                selectedDateRange.start = date
                selectedDateRange.end   = date
            }
            else if date.timeIntervalSince(selectedDateRange.start) == 0 && date.timeIntervalSince(selectedDateRange.end) == 0 {
                // there is just one selection & it has been deselected
                // do nothing
            }
            else if date.timeIntervalSince(selectedDateRange.start) == 0 {
                // the start date is selected again
                // do nothing
            }
            else if date.timeIntervalSince(selectedDateRange.end) == 0 {
                // the end date is selected again
                // do nothing
            }
            else if date.timeIntervalSince(selectedDateRange.end) > 0 {
                // end date and start date were same. and a date greater than end date is selected.
                // select a new end date
                if selectedDateRange.start.timeIntervalSince(selectedDateRange.end) == 0 {
                    selectedDateRange.end   = date
                    delegate?.didSelectStartDate(date: selectedDateRange.start, endDate: selectedDateRange.end)
                    if navigationController != nil {
                        _ = self.navigationController?.popViewController(animated: true)
                    }
                    else {
                        self.dismiss(animated: true, completion: nil)
                    }

                }
                else {
                    // a date greater than end date is selected
                    // assume seeker wants to start fresh
                    selectedDateRange.start = date
                    selectedDateRange.end   = date
                }
            }
            else if date.timeIntervalSince(selectedDateRange.start) < 0 {
                // a date lesser than start date is selected
                // assume seeker wants to start fresh
                selectedDateRange.start = date
                selectedDateRange.end = date
            }
        }
        // inform delegate
        if selectedDateRange != nil {
            
            // Alert if user selected today
            // 13732 - Remove the UX popup when user does same-day search
            /*
            if date.cr_truncateTime() == today.cr_truncateTime(){
                Utility.showAlert("", message: "Most caregivers are not available same day. Your results are now limited to caregivers who might be available same day.")
            }
             */

            if /* (pickerMode == .Boarding || pickerMode == .Sitting) && */ !singleDateSelection {
//                pickupTimeButton.isEnabled = (selectedDateRange.start != selectedDateRange.end)
            }
        }
        tableView.reloadData()
        // show time selection button
//        pickupTimeButton.isHidden = !(selectedDateRange != nil) || singleDateSelection

        startEndDateLabelContainer.isHidden = !(selectedDateRange != nil)
        monthSliderContainer.isHidden = !(selectedDateRange == nil)

        if selectedDateRange != nil /* && (pickerMode == .Boarding || pickerMode == .Sitting) */ && !singleDateSelection {
            selectedDateViewWidthConstraint.constant = 256
            startDateCenterConstraint?.isActive =  true
            if selectedDateRange.start.timeIntervalSince(selectedDateRange.end) == 0 {
                let dateComponenets1 = selectedDateRange.start.cr_daysInMonthAsTuple()
                startDateLabel.text = Date.monthFor(dateComponenets1.month).lowercased().SentenceCase + " \(dateComponenets1.day)"
                endDateLabel.text = "Select"
                endDateLabel.alpha = 0.5
            }
            else {
                let dateComponenets1 = selectedDateRange.start.cr_daysInMonthAsTuple()
                let dateComponenets2 = selectedDateRange.end.cr_daysInMonthAsTuple()
                startDateLabel.text = Date.monthFor(dateComponenets1.month).lowercased().SentenceCase + " \(dateComponenets1.day)"
                endDateLabel.text = Date.monthFor(dateComponenets2.month).lowercased().SentenceCase + " \(dateComponenets2.day)"
                endDateLabel.alpha = 1
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let startdateString = dateFormatter.string(from: selectedDateRange.start)
                let endDateString = dateFormatter.string(from: selectedDateRange.end)
                let startDay = getDayNameBy(stringDate: startdateString)
                let endDay = getDayNameBy(stringDate: endDateString)
                delegate?.didSelectDays!(startDay: startDay, endDay: endDay)
            }
        }
        else if let selectedDateRange = selectedDateRange {
            let dateComponenets1 = selectedDateRange.start.cr_daysInMonthAsTuple()
            startDateLabel.text = Date.monthFor(dateComponenets1.month).lowercased().SentenceCase + " \(dateComponenets1.day)"
            selectedDateViewWidthConstraint.constant = 132
            startDateCenterConstraint?.isActive = false
            endDateLabel.isHidden = true
            endDatePlaceholder.isHidden = true
            dateFormatter.dateFormat = "EEEE"
            startDatePlaceholder.text = dateFormatter.string(from: date).uppercased()
        }
        
        if singleDateSelection {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.searchButtonPressed(sender: nil)
            }
        }
    }

    func updateMonthLabelWithDate(date: Date) {
        dateFormatter.dateFormat = "MMMM"
        monthLabel.text = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "YYYY"
        yearLabel.text = dateFormatter.string(from: date)
        yearLabel.textColor = UIColor.colorWithHex(0x00BDA3)
    }

    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == tableView {
            let offset = scrollView.contentOffset
            let bounds = scrollView.bounds
            let size = scrollView.contentSize
            monthNavigateUpButton.isEnabled = !(offset.y < 10)
            monthNavigateDownButton.isEnabled = (offset.y + bounds.height < size.height)
            if let date = (tableView.visibleCells[3] as? WeekViewCell)?.firstDayOfWeek().cr_dateByAddingDays(3) {
                updateMonthLabelWithDate(date: date)
            }
        }
    }

    func selectedStartDate() -> Date? {
        return selectedDateRange != nil ? selectedDateRange.start : nil
    }

    func selectedEndDate() -> Date? {
        return selectedDateRange != nil ? selectedDateRange.end : nil
    }

    func showPickStartDatePill(cell: WeekViewCell) {
        let startDatePill = UILabel()
//        startDatePill.text = (pickerMode == .Grooming) ? "Pick a date" : "Pick a start date"
        startDatePill.text = "Pick a date"
        startDatePill.font = UIFont.init(name: "Avenir-Medium", size: 12)
        startDatePill.textAlignment = .center
        startDatePill.textColor = UIColor.white
        startDatePill.backgroundColor = UIColor.colorWithHex(0x00BDA3)
        startDatePill.clipsToBounds = true
        startDatePill.layer.cornerRadius = 13
        startDatePill.alpha = 0
        cell.contentView.addSubview(startDatePill)
        startDatePill.frame = CGRect(x: 0, y: 0, width: 108, height: 25)
        startDatePill.center.x = cell.gradientView.center.x
        startDatePill.center.y = cell.gradientView.center.y
        UIView.animate(withDuration: 0.857, animations: { () -> Void in
            startDatePill.alpha = 0.99
            }) { (finished) -> Void in
                if !finished {
                    if startDatePill.superview != nil {
                        startDatePill.removeFromSuperview()
                    }
                }
                UIView.animate(withDuration: 1.42, animations: { () -> Void in
                    startDatePill.alpha = 1
                    }) { (finished) -> Void in
                        if !finished {
                            if startDatePill.superview != nil {
                                startDatePill.removeFromSuperview()
                            }
                        }
                        UIView.animate(withDuration: 0.857, animations: { () -> Void in
                            startDatePill.alpha = 0
                            }, completion: { (finished) -> Void in
                                if startDatePill.superview != nil {
                                    startDatePill.removeFromSuperview()
                                }
                        })
                }
        }
    }

    func showPickEndDatePill(pos: CGPoint, cell: WeekViewCell) {
        let endDatePill = UILabel()
        endDatePill.text = "Select end date"
        endDatePill.font = UIFont.init(name: "Avenir-Medium", size: 12)
        endDatePill.textAlignment = .center
        endDatePill.textColor = UIColor.white
        endDatePill.backgroundColor = UIColor.colorWithHex(0x363B44)
        endDatePill.clipsToBounds = true
        endDatePill.layer.cornerRadius = 13
        endDatePill.alpha = 0
        cell.contentView.addSubview(endDatePill)
        endDatePill.frame = CGRect(x: 0, y: 0, width: 108, height: 25)
        endDatePill.center.x = pos.x
        endDatePill.center.y = cell.gradientView.center.y

        UIView.animate(withDuration: 0.857, animations: { () -> Void in
            endDatePill.alpha = 0.99
            }) { (finished) -> Void in
                UIView.animate(withDuration: 1.42, animations: { () -> Void in
                    endDatePill.alpha = 1
                    }) { (finished) -> Void in
                        UIView.animate(withDuration: 0.857, animations: { () -> Void in
                            endDatePill.alpha = 0
                            }, completion: { (finished) -> Void in
                                if endDatePill.superview != nil {
                                    endDatePill.removeFromSuperview()
                                }
                        })
                }
        }
    }

}
